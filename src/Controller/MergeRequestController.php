<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class MergeRequestController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $client   = $this->get('eight_points_guzzle.client.my_client');
        $session = new Session();
        $session->start();
        $session->get('name');

        $accessToken = $this->getParameter('token');

        $mrResponse = $client->get(
            '/api/v4//merge_requests',
            [
                'query' => [
                    'private_token' => $accessToken,
                    'scope' => 'all',
                    'state' => 'opened',
                    'wip' => 'no',
                    'per_page' => 500
                ],
            ]
        );

        $mr = json_decode($mrResponse->getBody()->getContents());

        $projectResponse  = $client->get(
            '/api/v4/projects',
            ['query' => [
                'private_token' => $accessToken,
                'per_page' => 500
            ]]
        );

        $projects = json_decode($projectResponse->getBody()->getContents(), true);
        $orderedProjects = [];

        foreach ($projects as $project) {
            if (!empty($session->get($project['name'], []))) {
                $orderedProjects[$project['id']] = $session->get($project['name']);
            } else {
                $orderedProjects[$project['id']] = $project;
                $labelsResponse =  $client->get(
                    '/api/v4/projects/' . $project['id'] . '/labels',
                    ['query' => ['private_token' => $accessToken]]
                );

                $labels = json_decode($labelsResponse->getBody()->getContents(), true);
                $colorLabels = [];
                foreach ($labels as $label) {
                    $colorLabels[$label['name']] = $label['color'];
                }

                $orderedProjects[$project['id']]['labels'] = $colorLabels;
                $session->set($project['name'], $orderedProjects[$project['id']]);
            }
        }

        return $this->render('merge-request/index.html.twig', [
            'requests' => $mr,
            'projects' => $orderedProjects,
        ]);
    }
}
